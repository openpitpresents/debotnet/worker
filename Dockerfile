FROM python:3.8-alpine

RUN mkdir -p /app
WORKDIR /app
COPY . .

RUN python3 -m pip install -U pip

RUN apk add --no-cache git gcc musl-dev && python3 -m pip install --editable . && apk del git gcc musl-dev
CMD sh app.sh
