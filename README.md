# De Botnet (Client / Worker component)

 - python 3.8+
 - some helper for virtualenvs is recommended
 
You will need credentials to a De Botnet [Server].

[Server]: https://gitlab.com/openpitpresents/debotnet/server

## Raw python setup

```bash
python3 -m pip install --editable .
env DBT_SERVER_ADDR=ws://localhost:8081 DBT_SERVER_TOKEN=token_go_here python3 -m debotnet_client 
```

## Environment variables

 - `DBT_SERVER_ADDR`: Server websocket address. Required.
 - `DBT_SERVER_TOKEN`: Server authentication token. Required.
 - `DBT_DEBUG`: Set to any value to enable debug logging.
 - `DBT_IP_LIST`: Set to the path of a JSON file containing IP addresses.
 - `DBT_HOSTNAME`: Set to a human-readable hostname, some identifying thing for
     introspection later on. Uses the system's hostname by default.

## Docker instructions

```
# Build:
docker build -t debotnet-client .

# Run:
docker run --env DBT_SERVER_ADDR=ws://localhost:8081 --env DBT_SERVER_TOKEN=token_goes_here -it --rm --name debotnet-client debotnet-client
```
