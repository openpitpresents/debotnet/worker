# debotnet-client: Distributed proxy (Worker component)
# Copyright 2020, lavatech and debotnet-client contributors
# SPDX-License-Identifier: GPL-3.0-only

import json
import aiohttp
import logging
from random import randint
from typing import Union, Any, Optional, List, Dict, Tuple

from websockets import WebSocketClientProtocol
from dataclasses import dataclass, field

log = logging.getLogger(__name__)


async def _recv(conn: WebSocketClientProtocol) -> Any:
    msg: Union[str, bytes] = await conn.recv()
    assert isinstance(msg, str)

    obj: Any = json.loads(msg)
    log.debug("received %r", obj)
    return obj


async def _send(conn, data):
    log.debug("sending %r", data)
    await conn.send(json.dumps(data))


class IPData:
    __slots__ = ("upstream_ip", "network_ip", "connector", "session")

    def __init__(self, upstream_ip, network_ip, connector, session):
        self.upstream_ip = upstream_ip
        self.network_ip = network_ip
        self.connector = connector
        self.session = session


@dataclass
class ClientContext:
    server_address: str
    conn: WebSocketClientProtocol

    # given by config
    ip_addresses: Optional[List[str]] = None

    # given by protocol
    upstream_ip_addresses: Optional[List[str]] = None

    _connectors: Dict[str, aiohttp.TCPConnector] = field(default_factory=dict)
    _sessions: Dict[str, aiohttp.ClientSession] = field(default_factory=dict)
    round_robin_counter: int = 0
    _network_map: Optional[Dict[str, str]] = None

    @property
    def network_map(self) -> Dict[str, str]:
        """Return a mapping from Protocol IP addresses to
        configured network IP addresses"""
        if self._network_map:
            return self._network_map

        assert self.upstream_ip_addresses is not None
        assert self.ip_addresses is not None

        self._network_map = {
            self.upstream_ip_addresses[index]: self.ip_addresses[index]
            for index in range(len(self.upstream_ip_addresses))
        }

        return self._network_map

    async def setup_ips(self):
        assert self.upstream_ip_addresses is not None
        assert self.ip_addresses is not None

        for upstream_ip, network_ip in self.network_map.items():
            log.debug("Creating connector at addr %r", network_ip)
            connector = aiohttp.TCPConnector(local_addr=(network_ip, 0))

            self._connectors[upstream_ip] = connector
            self._sessions[upstream_ip] = aiohttp.ClientSession(connector=connector)

    async def teardown(self):
        log.info("Destroying sessions and connectors")
        for session in self._sessions.values():
            await session.close()

    @property
    def random_ip(self) -> IPData:
        """Returns a random IP and a client session."""
        assert self.upstream_ip_addresses is not None
        assert self.ip_addresses is not None

        upstream_ip = self.upstream_ip_addresses[self.round_robin_counter]
        net_ip = self.ip_addresses[self.round_robin_counter]

        self.round_robin_counter = (self.round_robin_counter + 1) % len(
            self.upstream_ip_addresses
        )

        return IPData(
            upstream_ip,
            net_ip,
            self._connectors[upstream_ip],
            self._sessions[upstream_ip],
        )

    async def recv(self) -> Any:
        return await _recv(self.conn)

    async def send(self, obj: Any) -> None:
        return await _send(self.conn, obj)

    async def send_op(self, op, data) -> None:
        await self.send({"op": op.value, "d": data})
