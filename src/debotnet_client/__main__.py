# debotnet-client: Distributed proxy (Worker component)
# Copyright 2020, lavatech and debotnet-client contributors
# SPDX-License-Identifier: GPL-3.0-only

import os
import logging
import asyncio
import socket
import json
from typing import Optional

import aiohttp
import websockets

from .context import ClientContext
from .opcodes import Op
from .retry import ConnectionContext, handle_ws_error
from .utils import fetch_own_local_ip

log = logging.getLogger(__name__)

USERNAME_URL = "https://api.mojang.com/profiles/minecraft"
HAS_JOINED_URL = "https://sessionserver.mojang.com/session/minecraft/hasJoined"
DEBUG_IPINFO_URL = "https://ipinfo.io/json"


async def _send_work_failure(
    ctx: ClientContext, work_id: str, *, reason: Optional[str] = None
):
    log.warning("Work unit %r is marked as to-be-recovered. reason=%r", work_id, reason)
    await ctx.send_op(
        Op.WorkResponse,
        {
            "id": work_id,
            "recoverable": True,
            "failure_reason": reason or "<nothing>",
            "response": None,
        },
    )


async def _send_ip_ban(ctx, topic: str, upstream_ip: str) -> None:
    log.warning(
        "IP %r (%r) is marked as banned.", upstream_ip, ctx.network_map[upstream_ip]
    )
    await ctx.send_op(
        Op.Notify, {"t": "BANNED_IP", "data": {"ip": upstream_ip, "topic": topic}},
    )


async def _extract_json(resp):
    try:
        return await resp.json()
    except json.JSONDecodeError:
        return {"message": (await resp.read()).decode()}


async def _handle_username(ctx: ClientContext, work):
    work_id = work["id"]
    async with work["ip"].session.post(USERNAME_URL, json=work["data"]) as resp:
        # Only return recoverable if we're ratelimited/banned (HTTP 403)
        if resp.status == 403:
            await _send_work_failure(ctx, work_id, reason=f"Got {resp.status}")
            await _send_ip_ban(ctx, "username", work["ip"].upstream_ip)
            return

        result = await _extract_json(resp)
        log.debug("got response %s %r", work_id, result)
        await ctx.send_op(
            Op.WorkResponse,
            {"id": work_id, "status_code": resp.status, "response": result},
        )


async def _handle_has_joined(ctx, work):
    work_id = work["id"]
    async with work["ip"].session.get(HAS_JOINED_URL, params=work["data"]) as resp:
        # I'm not sure about fault conditions of hasJoined
        # defaulting to 403 check
        if resp.status == 403:
            await _send_work_failure(ctx, work_id, reason=f"Got {resp.status}")
            await _send_ip_ban(ctx, "has_joined", work["ip"].upstream_ip)
            return

        result = await _extract_json(resp)
        log.debug("got response %s %r", work_id, result)
        await ctx.send_op(
            Op.WorkResponse,
            {"id": work_id, "status_code": resp.status, "response": result},
        )


async def _handle_debug_ipinfo(ctx, work):
    work_id = work["id"]
    async with work["ip"].session.get(DEBUG_IPINFO_URL) as resp:
        if resp.status == 429:
            await _send_work_failure(ctx, work_id, reason=f"Got {resp.status}")
            await _send_ip_ban(ctx, "debug_ipinfo", work["ip"].upstream_ip)
            return

        result = await _extract_json(resp)
        log.debug("got response %s %r", work_id, result)
        await ctx.send_op(
            Op.WorkResponse,
            {"id": work_id, "status_code": resp.status, "response": result},
        )


async def _do_work(ctx, work_unit):
    assert work_unit["topic"] in ("username", "has_joined", "debug_ipinfo")
    data = work_unit["data"]

    ipdata = ctx.random_ip
    work_unit["ip"] = ipdata
    log.info(
        "Doing work %s with data %r (ip %r)",
        work_unit["topic"],
        data,
        ipdata.network_ip,
    )
    if work_unit["topic"] == "username":
        await _handle_username(ctx, work_unit)
    elif work_unit["topic"] == "has_joined":
        await _handle_has_joined(ctx, work_unit)
    elif work_unit["topic"] == "debug_ipinfo":
        await _handle_debug_ipinfo(ctx, work_unit)


async def _do_work_wrapper(ctx, work_unit):
    try:
        await _do_work(ctx, work_unit)
    except aiohttp.ClientConnectorError as exc:
        ipaddr = work_unit["ip"].network_ip
        log.warning(
            "Connector error. Maybe one of the IPs is not routing? (tried %r)", ipaddr
        )
        await _send_work_failure(ctx, work_unit["id"], reason=repr(exc))
    except Exception as exc:
        log.exception("Got error. Resending work unit to network")
        await _send_work_failure(ctx, work_unit["id"], reason=repr(exc))


async def _send_heartbeat_ack(ctx):
    log.debug("Heartbeating...")
    await ctx.send_op(Op.HeartbeatAck, None)


async def recv_loop(ctx):
    loop = asyncio.get_event_loop()

    while True:
        pkt = await ctx.recv()
        assert pkt["op"] in [3, 5]

        if pkt["op"] == 3:
            loop.create_task(_send_heartbeat_ack(ctx))
        elif pkt["op"] == 5:
            work_unit = pkt["d"]
            loop.create_task(_do_work_wrapper(ctx, work_unit))


async def worker_loop(server_address, conn, conn_ctx):
    ctx = ClientContext(server_address, conn)

    hello_pkt = await ctx.recv()
    assert hello_pkt["op"] == 0
    hello = hello_pkt["d"]
    log.info("Connected to %r", hello["hostname"])

    iplist_path = os.environ.get("DBT_IP_LIST")
    if iplist_path:
        with open(iplist_path, "r") as iplist_file:
            ctx.ip_addresses = json.load(iplist_file)
            log.info("Loaded %d addresses from file", len(ctx.ip_addresses))
    else:
        log.info("No file found, using local ip")
        ctx.ip_addresses = [await fetch_own_local_ip()]

    await ctx.send_op(
        Op.Identify,
        {
            "token": os.environ["DBT_SERVER_TOKEN"],
            "hostname": os.environ.get("DBT_HOSTNAME", socket.gethostname()),
            "ip_count": len(ctx.ip_addresses),
        },
    )

    ready = await ctx.recv()
    assert ready["op"] == 2
    ready_data = ready["d"]

    worker_id = ready_data["worker_id"]
    ip_addrs = ready_data["ip_addresses"]

    ctx.upstream_ip_addresses = ip_addrs
    await ctx.setup_ips()

    log.info("ready! worker ID %r", worker_id)

    conn_ctx.reset()
    try:
        await recv_loop(ctx)
    finally:
        await ctx.teardown()


# globals aren't elegant, but it is what it is.
# TODO move all of this to a Worker class.
RECONN_MAX_RETRIES = 20
conn_ctx = ConnectionContext()


async def async_main(server_address):
    global conn_ctx

    if conn_ctx.retries > RECONN_MAX_RETRIES:
        raise RuntimeError("Retried too much")

    try:
        conn = await websockets.connect(server_address)
        await worker_loop(server_address, conn, conn_ctx)
    except websockets.ConnectionClosedError as exc:
        conn_ctx.inc()
        await handle_ws_error(conn_ctx, exc)
        await async_main(server_address)
    except (OSError, IOError, websockets.WebSocketException):
        conn_ctx.inc()
        await handle_ws_error(conn_ctx, None)
        await async_main(server_address)


def main():
    try:
        server_address = os.environ["DBT_SERVER_ADDR"]
    except KeyError:
        print("Expected DBT_SERVER_ADDR environment variable")
        return

    loop = asyncio.get_event_loop()

    debug_flag = os.environ.get("DBT_DEBUG")
    if debug_flag is not None:
        log.level = logging.DEBUG
        logging.basicConfig(level=logging.DEBUG)
        logging.getLogger("websockets").setLevel(logging.INFO)
        log.info("on debug")
    else:
        logging.basicConfig(level=logging.INFO)

    try:
        loop.run_until_complete(async_main(server_address))
    except websockets.ConnectionClosedError as exc:
        log.error("Disconnected with code=%d, reason=%r", exc.code, exc.reason)
        # TODO reconnect


if __name__ == "__main__":
    main()
