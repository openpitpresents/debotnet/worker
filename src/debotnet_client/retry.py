# debotnet-client: Distributed proxy (Worker component)
# Copyright 2020, lavatech and debotnet-client contributors
# SPDX-License-Identifier: GPL-3.0-only

import asyncio
import logging
from dataclasses import dataclass
from random import uniform

log = logging.getLogger(__name__)

FORCE_RECONN = (4000, 4002, 4004)
FORCE_FAILURE = (4001, 4003, 4005)

RECONN_RETRY_CAP = 1
RECONN_RETRY_BASE = 0.7


@dataclass
class ConnectionContext:
    retries: int = 0

    def reset(self):
        self.retries = 0

    def inc(self):
        self.retries += 1


async def handle_ws_error(conn_ctx, exc) -> None:
    if exc is not None and exc.code in FORCE_FAILURE:
        log.error("Failed to connect. code=%d, reason=%r", exc.code, exc.reason)
        raise exc

    if exc is not None and exc.code in FORCE_RECONN:
        return

    sleep_secs = uniform(
        0, min(RECONN_RETRY_CAP, RECONN_RETRY_BASE * 2 ** conn_ctx.retries,),
    )
    log.exception("Failed to connect. Retrying in %.2fsec", sleep_secs)
    await asyncio.sleep(sleep_secs)
